const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    try {
      const data = UserService.getAll();
      return res.json(data);
    } catch (err) {
      res.err = err;
      res.status(404);
    } finally {
      next();
    }
  },
  responseMiddleware
);
router.get(
  "/:id",
  (req, res, next) => {
    try {
      res.data = UserService.search(req.params);
    } catch (err) {
      res.err = err;
      res.status(404);
    } finally {
      next();
    }
  },
  responseMiddleware
);
router.post(
  "/",
  //createUserValid,
  (req, res, next) => {
    const { firstName, lastName, email, phoneNumber, password } = req.body;
    try {
      res.data = UserService.createUser({
        firstName,
        lastName,
        email,
        phoneNumber,
        password,
      });
    } catch (err) {
      res.err = err;
      res.status(400);
    } finally {
      next();
    }
  },
  responseMiddleware
);
router.delete(
  "/:id",
  (req, res, next) => {
    try {
      res.data = UserService.createUser(req.params);
    } catch (err) {
      res.err = err;
      res.status(404);
    } finally {
      next();
    }
  },
  responseMiddleware
);
module.exports = router;
