const responseMiddleware = (req, res, next) => {
  if (res.err) {
    res.json({
      error: true,
      message: res.err,
    });
  }
  res.status(200).json(res.data);
};

exports.responseMiddleware = responseMiddleware;
