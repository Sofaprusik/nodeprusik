const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { firstName, lastName, email, phoneNumber, password } = req.body;
  try {
    const user = UserService.getOne({ name });
    if (user) {
      res.status(400);
      console.log(`User with email '${email}' already exists`);
      res.err = `User with email '${email}' already exists`;
      return next();
    }
  } catch (err) {}
  if (!firstName || firstName === "") {
    res.status(400);
    res.err = "Firstname cannot be an empty string";
    return next();
  }
  if (!lastName || lastName === "") {
    res.status(400);
    res.err = "Lastname cannot be an empty string";
    return next();
  }
  if (!phoneNumber || !/\+380\d{9}/g.test(phoneNumber)) {
    res.status(400);
    res.err = "Phonenumber should be +380xxxxxxxxx";
    return next();
  }
  if (!email || !/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/.test(email)) {
    res.status(400);
    res.err = "Email address should be gmail";
    return next();
  }
  if (!password || password.length < 3) {
    res.status(400);
    res.err = "Password should contain min 3 symbols";
    return next();
  }
  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
