const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  getAll() {
    const fighter = FighterRepository.getAll();
    if (!fighter) {
      throw Error("User not found");
    }
    return fighter || [];
  }
  createFighter(data) {
    const fighter = FighterRepository.create(data);
    if (fighter) {
      return FighterRepository.create(data);
    } else {
      return null;
    }
  }
  deleteFighter(id) {
    const fighter = FighterRepository.delete(id);
    if (!fighter) throw Error("User was not deleted");
    return fighter;
  }
  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
