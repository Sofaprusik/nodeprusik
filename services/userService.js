const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  getAll() {
    const user = UserRepository.getAll();
    return user || [];
  }
  createUser(data) {
    const user = UserRepository.create(data);
    if (user) {
      return UserRepository.create(data);
    } else {
      return null;
    }
  }
  deleteUser(id) {
    const user = UserRepository.delete(id);
    if (!user) throw Error("User was not found");
    return user;
  }
  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
